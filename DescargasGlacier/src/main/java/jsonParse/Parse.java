package jsonParse;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Parse {
	static String RUTA = "./data/";
	
	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
		JSONParser parser = new JSONParser();
		
		JSONObject inventario = (JSONObject) parser.parse(new FileReader(RUTA+"inventario.json"));
		JSONArray archivos = (JSONArray) inventario.get("ArchiveList");
		
		String ids="";
		for (Object obj : archivos){
			JSONObject archivo = (JSONObject) obj;
			ids+=archivo.get("ArchiveId")+","+archivo.get("ArchiveId")+"\n";
			//ids+=RUTA+(String)archivo.get("ArchiveDescription")+","+(long)archivo.get("ArchiveId");
		}
		ids=ids.trim();
		BufferedWriter bw = new BufferedWriter(new FileWriter(RUTA+"nodescargados.txt"));
        bw.write(ids);
        bw.close();
        bw = new BufferedWriter(new FileWriter(RUTA+"nosolicitados.txt"));
        bw.write(ids);
		bw.close();
	}
}