/**
 * 
 */
package downloader;

import java.awt.FontFormatException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Semaphore;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.security.cert.CertificateNotYetValidException;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.glacier.AmazonGlacierClient;
import com.amazonaws.services.glacier.model.GetJobOutputRequest;
import com.amazonaws.services.glacier.model.GetJobOutputResult;
import com.amazonaws.services.glacier.model.InitiateJobRequest;
import com.amazonaws.services.glacier.model.InitiateJobResult;
import com.amazonaws.services.glacier.model.JobParameters;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.DeleteTopicRequest;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.amazonaws.services.sns.model.UnsubscribeRequest;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Downloader extends Thread{
	
	//TODO cambiar rutas de archivos a rutas din�micas
	
	//Atributos de distribuci�n de carga
	private boolean fin;
	private int numHilo;
	private Semaphore turno;
	
	//Atributos de descarga
	public static String vaultName = "testvault";
    public static String region   = "us-west-2";
    public static String snsTopicName;
    public static String sqsQueueName;
    //public static long downloadChunkSize = 4194304; // 4 MB  
    public static String sqsQueueARN;
    public static String sqsQueueURL;
    public static String snsTopicARN;
    public static String snsSubscriptionARN;
    public static long sleepTime  = 5000*60; //pregunta por el job cada 5 minutos
    
    public static AmazonGlacierClient client;
    public static AmazonSQSClient sqsClient;
    public static AmazonSNSClient snsClient; 
	
	/**
	 * Metodo que representa un hilo de descarga
	 */
	public Downloader(Semaphore t, int n) throws Exception {
		fin=false;
		numHilo=n;
		turno=t;
		
		snsTopicName="topic"+n;
		sqsQueueName="queue"+n;
		
		this.start();
	}

	/**
	 * Solicita y ejecuta descargas del Pool hasta que no queden registros de archivos por descargar
	 */
	@Override
	public void run() {
		while (true){
			try {
				turno.acquire();
				String archivoASolicitar=darArchivoNoSolicitado();
				turno.release();
				if (archivoASolicitar==null) break;
				
				
				String archiveId=archivoASolicitar.split(",")[1];
				String filename="./data/descargados/"+archivoASolicitar.split(",")[0];
				//Inicia el proceso de descarga
				
				ProfileCredentialsProvider credentials = new ProfileCredentialsProvider();

		        client = new AmazonGlacierClient(credentials);
		        client.setEndpoint("https://glacier." + region + ".amazonaws.com");
		        sqsClient = new AmazonSQSClient(credentials);
		        sqsClient.setEndpoint("https://sqs." + region + ".amazonaws.com");
		        snsClient = new AmazonSNSClient(credentials);
		        snsClient.setEndpoint("https://sns." + region + ".amazonaws.com");
		        
		        try {
		            setupSQS();
		            
		            setupSNS();

		            String jobId = initiateJobRequest(archiveId);
		            System.out.println("Jobid = " + jobId+", Archiveid = "+archiveId);
		            
		            boolean recibido = waitForJobToComplete(jobId, sqsQueueURL);
		            if (!recibido) { 
		            	turno.acquire();
		            	marcarNoSolicitado(filename,archiveId);
		            	turno.release();
		            	throw new Exception("Job did not complete successfully."); }
		            
		            downloadJobOutput(jobId,filename);
		            turno.acquire();
		            marcarDescargado(archiveId);
		            turno.release();
		            cleanUp();
		            
		        } catch (Exception e) {
		            System.err.println("Archive retrieval failed.");
		            System.err.println(e);
		            cleanUp();
		            break;
		        }
				
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}
		}
	}
	
	private void marcarDescargado(String archiveId) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader("./data/nodescargados.txt"));
		String valoresArchivo;
		String nuevo="";
		
		//Removemos el archivo descargado de los no descargados
        while ((valoresArchivo=br.readLine()) != null) {
        	if (!valoresArchivo.split(",")[1].equals(archiveId)) nuevo+=valoresArchivo+"\n";
        	}
        if (!nuevo.equals("")) nuevo=nuevo.substring(0,nuevo.length()-1);
        
        BufferedWriter bw = new BufferedWriter(new FileWriter("./data/nodescargados.txt"));
        bw.write(nuevo);
		
        br.close();bw.close();
	}

	private void marcarNoSolicitado(String filename, String archiveId) throws Exception {
		BufferedReader br = new BufferedReader(new FileReader("./data/nosolicitados.txt"));
		String valoresArchivo;
		String nuevo="";

        while ((valoresArchivo=br.readLine()) != null) {
           nuevo+=valoresArchivo+"\n";}
        if (!nuevo.equals("")) nuevo=nuevo.substring(0,nuevo.length()-1);
        nuevo+=filename+","+archiveId;
        
        BufferedWriter bw = new BufferedWriter(new FileWriter("./data/nosolicitados.txt"));
        bw.write(nuevo);
		
        br.close();bw.close();
	}

	private String darArchivoNoSolicitado() throws Exception{
		BufferedReader br = new BufferedReader(new FileReader("./data/nosolicitados.txt"));
		String valoresArchivo=br.readLine();
		String solicitado=valoresArchivo;
		String nuevo="";

		//Removemos el archivo seleccionado de los no solicitados
        while ((valoresArchivo=br.readLine()) != null) {
           nuevo+=valoresArchivo+"\n";}
        if (!nuevo.equals("")) nuevo=nuevo.substring(0,nuevo.length()-1);
        
        BufferedWriter bw = new BufferedWriter(new FileWriter("./data/nosolicitados.txt"));
        bw.write(nuevo);
		
        br.close();bw.close();
		return solicitado;
	}
	
	/**
	 * M�todos extra�dos de los ejemplos de Amazon
	 */
	
	private static void setupSQS() {
        CreateQueueRequest request = new CreateQueueRequest()
            .withQueueName(sqsQueueName);
        CreateQueueResult result = sqsClient.createQueue(request);  
        sqsQueueURL = result.getQueueUrl();
                
        GetQueueAttributesRequest qRequest = new GetQueueAttributesRequest()
            .withQueueUrl(sqsQueueURL)
            .withAttributeNames("QueueArn");
        
        GetQueueAttributesResult qResult = sqsClient.getQueueAttributes(qRequest);
        sqsQueueARN = qResult.getAttributes().get("QueueArn");
    }
	
	private static void setupSNS() {
        CreateTopicRequest request = new CreateTopicRequest()
            .withName(snsTopicName);
        CreateTopicResult result = snsClient.createTopic(request);
        snsTopicARN = result.getTopicArn();

        SubscribeRequest request2 = new SubscribeRequest()
            .withTopicArn(snsTopicARN)
            .withEndpoint(sqsQueueARN)
            .withProtocol("sqs");
        SubscribeResult result2 = snsClient.subscribe(request2);
                
        snsSubscriptionARN = result2.getSubscriptionArn();
    }
    private static String initiateJobRequest(String archiveId) {
        
        JobParameters jobParameters = new JobParameters()
            .withType("archive-retrieval")
            .withArchiveId(archiveId)
            .withSNSTopic(snsTopicARN);
            //.withTier("Expedited");
        
        InitiateJobRequest request = new InitiateJobRequest()
            .withVaultName(vaultName)
            .withJobParameters(jobParameters);
        
        InitiateJobResult response = client.initiateJob(request);
        
        return response.getJobId();
    }
    
    private static boolean waitForJobToComplete(String jobId, String sqsQueueUrl) throws InterruptedException, JsonParseException, IOException {
        
        Boolean messageFound = false;
        Boolean jobSuccessful = false;
        long archiveSizeInBytes = -1;
        ObjectMapper mapper = new ObjectMapper();
        JsonFactory factory = mapper.getFactory();
        
        while (!messageFound) {
            List<Message> msgs = sqsClient.receiveMessage(
               new ReceiveMessageRequest(sqsQueueUrl).withMaxNumberOfMessages(10)).getMessages();

            if (msgs.size() > 0) {
                for (Message m : msgs) {
                    JsonParser jpMessage = factory.createJsonParser(m.getBody());
                    JsonNode jobMessageNode = mapper.readTree(jpMessage);
                    String jobMessage = jobMessageNode.get("Message").textValue();
                    
                    JsonParser jpDesc = factory.createJsonParser(jobMessage);
                    JsonNode jobDescNode = mapper.readTree(jpDesc);
                    String retrievedJobId = jobDescNode.get("JobId").textValue();
                    String statusCode = jobDescNode.get("StatusCode").textValue();
                    archiveSizeInBytes = jobDescNode.get("ArchiveSizeInBytes").longValue();
                    if (retrievedJobId.equals(jobId)) {
                        messageFound = true;
                        if (statusCode.equals("Succeeded")) {
                            jobSuccessful = true;
                        }
                    }
                }
                
            } else {
              Thread.sleep(sleepTime); 
            }
          }
        return (messageFound && jobSuccessful);
    }
    
    private static void downloadJobOutput(String jobId, String fileName) throws IOException {
        
        GetJobOutputRequest getJobOutputRequest = new GetJobOutputRequest()
            .withVaultName(vaultName)
            .withJobId(jobId);
        GetJobOutputResult getJobOutputResult = client.getJobOutput(getJobOutputRequest);
    
        FileWriter fstream = new FileWriter(fileName);
        BufferedWriter out = new BufferedWriter(fstream);
        BufferedReader in = new BufferedReader(new InputStreamReader(getJobOutputResult.getBody()));            
        String inputLine;
        try {
            while ((inputLine = in.readLine()) != null) {
                out.write(inputLine);
            }
        }catch(IOException e) {
            throw new AmazonClientException("Unable to save archive", e);
        }finally{
            try {in.close();}  catch (Exception e) {}
            try {out.close();}  catch (Exception e) {}             
        }
        System.out.println("Retrieved archive to " + fileName);
    }
    
	private static void cleanUp() {
        snsClient.unsubscribe(new UnsubscribeRequest(snsSubscriptionARN));
        snsClient.deleteTopic(new DeleteTopicRequest(snsTopicARN));
        sqsClient.deleteQueue(new DeleteQueueRequest(sqsQueueURL));
    }
}
