package downloader;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.security.Security;
import java.util.concurrent.Semaphore;

public class Pool {

	/**
	 * Constante que especifica el numero de threads que se usan en el pool de conexiones.
	 */
	public static final int N_THREADS = 20;
	
	/**
	 * inicializa un pool de threads determinado por la constante nThreads.
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {

		Semaphore turno = new Semaphore(1);
		
		// Genera n threads que correran durante la sesion.
		Downloader [] threads = new Downloader[N_THREADS];
		for ( int i = 0 ; i < N_THREADS ; i++) {
			try {
				threads[i] = new Downloader(turno,i);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
